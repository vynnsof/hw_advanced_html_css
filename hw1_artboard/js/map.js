// // initMap() - функция инициализации карты
// function initMap() {
//
//     // Координаты центра на карте. Широта: 56.2928515, Долгота: 43.7866641
//     var centerLatLng = new google.maps.LatLng(50.4288159, 30.5931793);
//     // Обязательные опции с которыми будет проинициализированна карта
//     var mapOptions = {
//         center: centerLatLng, // Координаты центра мы берем из переменной centerLatLng
//         zoom: 15               // Зум по умолчанию. Возможные значения от 0 до 21
//     };
//     // Создаем карту внутри элемента #map
//     var map = new google.maps.Map(document.getElementById("map"), mapOptions);
//     var styles =[
//         {
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#252525"
//                 }
//             ]
//         },
//         {
//             "elementType": "labels.icon",
//             "stylers": [
//                 {
//                     "visibility": "off"
//                 }
//             ]
//         },
//         {
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#757575"
//                 }
//             ]
//         },
//         {
//             "elementType": "labels.text.stroke",
//             "stylers": [
//                 {
//                     "color": "#181818"
//                 }
//             ]
//         },
//         {
//             "featureType": "administrative.land_parcel",
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#181818"
//                 }
//             ]
//         },
//         {
//             "featureType": "poi",
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#181818"
//                 }
//             ]
//         },
//         {
//             "featureType": "poi",
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#757575"
//                 }
//             ]
//         },
//         {
//             "featureType": "poi.park",
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#3C3C3C5"
//                 }
//             ]
//         },
//         {
//             "featureType": "poi.park",
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#9e9e9e"
//                 }
//             ]
//         },
//         {
//             "featureType": "road",
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#373737"
//                 }
//             ]
//         },
//         {
//             "featureType": "road.arterial",
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#757575"
//                 }
//             ]
//         },
//         {
//             "featureType": "road.highway",
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#3C3C3C"
//                 }
//             ]
//         },
//         {
//             "featureType": "road.highway",
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#616161"
//                 }
//             ]
//         },
//         {
//             "featureType": "road.local",
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#9e9e9e"
//                 }
//             ]
//         },
//         {
//             "featureType": "transit.line",
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#3C3C3C"
//                 }
//             ]
//         },
//         {
//             "featureType": "transit.station",
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#eeeeee"
//                 }
//             ]
//         },
//         {
//             "featureType": "water",
//             "elementType": "geometry",
//             "stylers": [
//                 {
//                     "color": "#000000"
//                 }
//             ]
//         },
//         {
//             "featureType": "water",
//             "elementType": "labels.text.fill",
//             "stylers": [
//                 {
//                     "color": "#9e9e9e"
//                 }
//             ]
//         }
//     ]
//     map.setOptions({styles: styles});
//     // Добавляем маркер
//     var marker = new google.maps.Marker({
//         position: centerLatLng,            // Координаты расположения маркера. В данном случае координаты нашего маркера совпадают с центром карты, но разумеется нам никто не мешает создать отдельную переменную и туда поместить другие координаты.
//         map: map,     // Карта на которую нужно добавить маркер
//         title: "1 Pavla Tychyny Ave, Kiev, Ukraine", // (Необязательно) Текст выводимый в момент наведения на маркер
//         icon: "img/pin.png"
//     });
//
//
// }
// // Ждем полной загрузки страницы, после этого запускаем initMap()
// google.maps.event.addDomListener(window, "load", initMap);