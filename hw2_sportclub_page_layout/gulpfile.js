const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const minifyJS = require('gulp-minify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const notify = require('gulp-notify');

//clean build task
gulp.task('clean-dist', function () {
    return gulp.src('./dist/', {read: false})
        .pipe(clean());
});

// html task
gulp.task('copy-html', function () {
    return gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.reload({stream: true}));
});

// CSS task
gulp.task('styles', function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}).on("error", notify.onError()))
        .pipe(rename({suffix: '.min', prefix: ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS({level: {1: {specialComments: 0}}}))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream())
});

//JS task
gulp.task('scripts', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(minifyJS())
        .pipe(concat('scripts.min.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.reload({stream: true}))
});

//imagemin

gulp.task('imagemin', () =>
    gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img/'))
);


// Watch on everything
gulp.task('dev', function () {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('./src/*.html', gulp.parallel('copy-html'));
    gulp.watch('./src/scss/**/*.scss', gulp.parallel('styles'));
    gulp.watch('./src/js/**/*.js', gulp.parallel('scripts'));

});

gulp.task('build', gulp.series('clean-dist', 'styles', ['copy-html', 'scripts'], 'imagemin'));